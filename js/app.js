// Dom7
var $ = Dom7;

// Init App
var app = new Framework7({
  id: 'io.fmtjek.app',
  root: '#app',
  theme: "ios",
  routes: routes,
  vi: {
    placementId: 'pltd4o7ibb9rc653x14',
  },
});

/*
* Define login method
*/

function signIn(element){

  localStorage.setItem('username', jQuery('input[name="username"]').val())
  var settings = {
    "async": true,
    "crossDomain": true,
    "url": "https://ksapp.fm3kpi-test.dk/webservice.php",
    "method": "POST",
    "headers": {
      "content-type": "application/x-www-form-urlencoded",
      "cache-control": "no-cache",
      "postman-token": "83cfc812-a94d-abe5-bc64-db63ac5c0308"
    },
    "data": {
      "action": "login",
      "username": jQuery('input[name="username"]').val(),
      "password": jQuery('input[name="password"]').val()
    },
    "beforeSend": function() {
        app.progressbar.show()
        app.preloader.show()
    },
    "complete": function() {
        app.progressbar.hide()
        app.preloader.hide()
    }
  }

  jQuery.ajax(settings).done(function (response) {
    var data = JSON.parse(response)

    var toastIcon = app.toast.create({
      icon: '<i class="f7-icons">check_round_fill</i>',
      text: data.message[0],
      position: 'center',
      closeTimeout: 2000,
    });
    toastIcon.open()

  });

}

// Option 2. Using live 'page:init' event handlers for each page
$(document).on('page:init', '.page[data-name="about"]', function (e) {
  // Do something here when page with data-name="about" attribute loaded and initialized
  jQuery('.block.block-strong span').html(localStorage.setItem('username'))
})